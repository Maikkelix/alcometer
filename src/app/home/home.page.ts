import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  weight: number;  
  genders = [];
  gender: string;
  ongoing_time = [];
  time: number;
  consumed_bottles = [];
  bottles: number;
  promilles: number;  
  litres: number;
  grams: number;
  grams_to_burn: number;
  grams_still_left: number;
  
  constructor() { }

  ngOnInit() {
    //this.ongoing_time = [];
    //this.consumed_bottles = [];

    this.genders.push('Male');
    this.genders.push('Female');

    this.ongoing_time = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];

    //this.ongoing_time.push(1);
    //this.ongoing_time.push(2);
    //this.ongoing_time.push(3);
    //this.ongoing_time.push(4);
    //this.ongoing_time.push(5);
    //this.ongoing_time.push(6);
    //this.ongoing_time.push(7);
    //this.ongoing_time.push(8);
    //this.ongoing_time.push(9);
    //this.ongoing_time.push(10);
    //this.ongoing_time.push(11);
    //this.ongoing_time.push(12);
    //this.ongoing_time.push(13);
    //this.ongoing_time.push(14);
    //this.ongoing_time.push(15);

    this.consumed_bottles.push(1);
    this.consumed_bottles.push(2);
    this.consumed_bottles.push(3);
    this.consumed_bottles.push(4);
    this.consumed_bottles.push(5);
    this.consumed_bottles.push(6);
    this.consumed_bottles.push(7);
    this.consumed_bottles.push(8);
    this.consumed_bottles.push(9);
    this.consumed_bottles.push(10);
    this.consumed_bottles.push(15);
    this.consumed_bottles.push(20);
    this.consumed_bottles.push(24);

    this.gender = 'Male';
    this.time = 1;
    this.bottles = 4;
    this.weight=80;
	
  }

  calculate() {

	//alert('calculate starting');				
    let litres = +this.consumed_bottles * 0.33;	
    let grams = litres * +this.consumed_bottles * 4.5; //4.5 is the percentages of the bottle
    let grams_to_burn = this.weight / 10;
    let grams_still_left = grams - (grams_to_burn * +this.ongoing_time);
	//alert(litres+','+grams+','+grams_to_burn+','+grams_still_left+','+this.gender);
	let proms=0;
    if (this.gender === 'Male') {
      proms = grams_still_left / (this.weight * 0.7);
    } else {
      proms = grams_still_left / (this.weight * 0.6);
    }		
	// negative promilles not shown
	if (proms > 0) {
		this.promilles = proms;
	} 
	else 
	{
		this.promilles=0.0;
	};	
	//alert('calculate finished');	
  }  
  
  /*
  calculate_old() {
    let litres = this.bottles * 0.33;
    let grams = this.litres * this.bottles * 4.5; //4.5 is the percentages of the bottle
    let grams_to_burn = this.weight / 10;
    let grams_still_left = this.grams - (this.grams_to_burn * this.time);

    if (this.gender === 'Male') {
      this.promilles = this.grams_still_left / (this.weight * 0.7);
    } else {
      this.promilles = this.grams_still_left / (this.weight * 0.6);
    }	
  } 
  */ 
}
